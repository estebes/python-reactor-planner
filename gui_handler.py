import os
import time
import reaktor_components
from PyQt5.QtCore import QObject, pyqtSlot, QStandardPaths, QMetaObject, Qt, Q_ARG, QVariant
from imgurpython import ImgurClient
from damageable_component import DamageableComponent
from datetime import datetime
from reaktor import Reactor


class GuiHandler(QObject):
    def __init__(self, application, engine):
        QObject.__init__(self)
        self.application = application
        self.engine = engine
        self.reactor = Reactor()
        self.components = ["", "single_uranium_rod", "dual_uranium_rod", "quad_uranium_rod", "single_mox_rod",
                           "dual_mox_rod", "quad_mox_rod", "reactor_plating", "heat_capacity_reactor_plating",
                           "containment_reactor_plating", "heat_vent", "reactor_heat_vent", "overclocked_heat_vent",
                           "advanced_heat_vent", "component_heat_vent", "heat_exchanger", "reactor_heat_exchanger",
                           "component_heat_exchanger", "advanced_heat_exchanger", "rsh_condensator", "lzh_condensator",
                           "neutron_reflector", "thick_neutron_reflector", "iridium_neutron_reflector",
                           "coolant_cell_10k", "coolant_cell_30k", "coolant_cell_60k"]

    @pyqtSlot(int, int)
    def add_to_reactor(self, index, component_index):
        component_name = self.components[component_index]
        component = getattr(reaktor_components, component_name)()

        self.reactor.add_component_gui(index, component)

        durability = isinstance(component, DamageableComponent)

        qml_window = self.engine.rootObjects()
        output = qml_window[0].findChild(QObject, "function_handler")
        QMetaObject.invokeMethod(output, "add_component_to_reactor", Qt.DirectConnection, Q_ARG(QVariant, index),
                                 Q_ARG(QVariant, component_name), Q_ARG(QVariant, durability))

    @pyqtSlot(int)
    def remove_from_reactor(self, index):
        self.reactor.remove_component_gui(index)

    @pyqtSlot()
    def perform_reactor_simulation(self):
        simulation = self.reactor.simulate()
        output_grid = simulation[0]
        console_output = simulation[1]

        qml_window = self.engine.rootObjects()
        output = qml_window[0].findChild(QObject, "function_handler")

        index = 0
        while output_grid:
            QMetaObject.invokeMethod(
                output, "update_durability", Qt.DirectConnection, Q_ARG(QVariant, index),
                Q_ARG(QVariant, output_grid.pop()))
            index += 1

        while console_output:
            QMetaObject.invokeMethod(
                output, "print_to_console", Qt.DirectConnection, Q_ARG(QVariant, console_output.popleft()))

    @pyqtSlot(bool)
    def take_print_screen(self, upload):
        # File name
        file_name = datetime.now().strftime("%Y-%m-%d_%H.%M.%S")

        # File path
        # noinspection PyArgumentList
        file_path = QStandardPaths.writableLocation(QStandardPaths.PicturesLocation) + "/"

        # Grab window
        image = self.application.primaryScreen().grabWindow(self.application.allWindows()[0].winId())
        image.save(file_path + file_name, "png")

        # If upload == true -> upload to imgur
        if upload:
            self.upload_print_screen(file_path + file_name)

    def upload_print_screen(self, file):
        # Imgur Client ID
        client_id = "5c0fcc3e8d99e2c"

        # Imgur Client Secret
        client_secret = "c10093b2df171838a4c70e6a6f465f2ec175d3a2"

        # Stuff
        client = ImgurClient(client_id, client_secret)

        # Wait for file to be created
        while not os.path.exists(file):
            time.sleep(1)

        # Upload image (no config is needed and the upload is anon)
        image = client.upload_from_path(file, config=None, anon=True)

        # Uploaded image url
        qml_window = self.engine.rootObjects()
        output = qml_window[0].findChild(QObject, "function_handler")
        QMetaObject.invokeMethod(output, "print_to_console", Qt.DirectConnection, Q_ARG(QVariant, image["link"]))
