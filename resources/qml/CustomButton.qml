import QtQuick 2.5
import QtGraphicalEffects 1.0

Item {
    visible: true
    id: button

    property string text: ""
    signal click();

    Rectangle {
        id: background

        implicitWidth: 200
        implicitHeight: 50

        color: "#000000";

        onEnabledChanged: state = ""

        Rectangle {
            width: parent.width - 4;
            height: parent.height - 4;
            anchors.centerIn: parent;
            color: "#6F6F6F";

            Rectangle {
                width: parent.width - 3;
                height: parent.height - 3;
                color: "#AAAAAA";
            }

            Rectangle {
                width: parent.width - 3;
                height: parent.height - 3;
                x: 3;
                y: 3;
                color: "#565656";
            }

            Rectangle {
                width: parent.width - 6;
                height: parent.height - 6;
                x: 3;
                y: 3;
                color: "#6F6F6F"
            }
        }

        Rectangle {
            id: overlay;
            width: parent.width - 4;
            height: parent.height - 4;
            anchors.centerIn: parent;
            color: "#3B5FFF";
            opacity: 0.0;
        }

        Image {
            width: parent.width - 4;
            height: parent.height - 4;
            anchors.centerIn: parent;
            source: "qrc:///resources/textures/noise.png";
            opacity: 0.2;
        }

        Text {
            id: text;

            renderType: Text.NativeRendering;

            FontLoader {
                id: menuFont;
                source: "qrc:///resources/fonts/menu_font.ttf";
            }

            layer.enabled: true;
            layer.effect: DropShadow {
                horizontalOffset: 3;
                verticalOffset: 3;
                radius: 0;
                samples: 0;
                spread: 0.0;
                color: "#383838";
            }

            text: button.text;
            anchors.centerIn: parent;
            color: "#FFFFFF";

            font.family: menuFont.name;
            font.pointSize: 15;
        }

        MouseArea {
            id: handler
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                if(background.state != "Clicked") {
                    background.state = "Hovering"
                }
            }
            onExited: {
                if(background.state != "Clicked") {
                    background.state = ""
                }
            }
        }

        Component.onCompleted: {
            handler.clicked.connect(click);
        }

        states: [
            State {
                name: "Hovering"
                PropertyChanges {
                    target: overlay;
                    opacity: 0.4;
                }

                PropertyChanges {
                    target: text;
                    color: "#FFFF00";
                }
            },

            State {
                name: "Clicked"
                PropertyChanges {
                    target: overlay;
                    opacity: 0.4;
                }

                PropertyChanges {
                    target: text;
                }
            }
        ]
    }
}



