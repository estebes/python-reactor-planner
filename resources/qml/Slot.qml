import QtQuick 2.5

Item {
    id: slot;
    implicitWidth: parent.width;
    implicitHeight: parent.height;
    width: parent.width;
    height: parent.width;

    onEnabledChanged: state = ""
    signal leftclick();
    signal rightclick();

    Rectangle {
        width: parent.width;
        height: parent.height;
        anchors.centerIn: parent;
        color: "#8B8B8B";
    }

    Rectangle {
        width: parent.width - 3;
        height: parent.height - 3;
        color: "#373737";
    }

    Rectangle {
        width: parent.width - 3;
        height: parent.height - 3;
        x: 3;
        y: 3;
        color: "#FFFFFF";
    }

    Rectangle {
        id: background;
        width: parent.width - 6;
        height: parent.height - 6;
        x: 3;
        y: 3;
        color: "#8B8B8B"
    }

    Image {
        id: item;
        anchors.centerIn: parent;
        width: parent.width - 12;
        height: parent.height - 12;
        fillMode: Image.PreserveAspectFit;
        smooth: false;
        source: "";

        property bool durability: false;

        Rectangle {
            visible: item.durability;
            id: item_durability;
            width: parent.width;
            height: 6;
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.bottom: parent.bottom;
            anchors.bottomMargin: 0;
            color: "#000000"

            Rectangle {
                width: parent.width - 3;
                height: 3;
                color: "#3F4000"
            }

            Rectangle {
                id: item_durability_bar
                width: parent.width - 3;
                height: 3;
                color: "#01FE00"
            }
        }
    }

    MouseArea {
        id: handler;
        anchors.fill: parent;
        hoverEnabled: true;
        acceptedButtons: Qt.LeftButton | Qt.RightButton;
        onClicked: {
            if(mouse.button & Qt.LeftButton) {
                slot.leftclick();
            }
            else if(mouse.button & Qt.RightButton) {
                slot.rightclick();
            }
        }
        onEntered: {
            slot.state = "Hovering";
        }
        onExited: {
            slot.state = "";
        }
    }

    states: [
        State {
            name: "Hovering"
            PropertyChanges {
                target: background;
                color: "#C6C6C6";
            }
        }
    ]

    function add_component(component, durability) {
        if (component.length !== 0 && component !== "") {
            item.source = "qrc:///resources/textures/items/" + component + ".png";
            if (durability) {
                item_durability_bar.width = (item_durability.width - 3);
                item.durability = true;
            }
        }
        else {
            remove_component();
        }
    }

    function remove_component() {
        item.source = "";
        item.durability = false;
    }

    function update_durability(durability) {
        item_durability_bar.width = (item_durability.width - 3) * durability;
    }
}
