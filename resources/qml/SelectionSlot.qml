import QtQuick 2.5

Item {
    id: slot;
    implicitWidth: parent.width;
    implicitHeight: parent.height;
    width: parent.width;
    height: parent.width;

    onEnabledChanged: state = ""
    signal click();

    Rectangle {
        width: parent.width;
        height: parent.height;
        anchors.centerIn: parent;
        color: "#8B8B8B";
    }

    Rectangle {
        width: parent.width - 3;
        height: parent.height - 3;
        color: "#373737";
    }

    Rectangle {
        width: parent.width - 3;
        height: parent.height - 3;
        x: 3;
        y: 3;
        color: "#FFFFFF";
    }

    Rectangle {
        id: background;
        width: parent.width - 6;
        height: parent.height - 6;
        x: 3;
        y: 3;
        color: "#8B8B8B"
    }

    Image {
        id: item;
        anchors.centerIn: parent;
        width: parent.width - 12;
        height: parent.height - 12;
        fillMode: Image.PreserveAspectFit;
        smooth: false;
        source: "";
    }

    MouseArea {
        id: handler;
        anchors.fill: parent;
        hoverEnabled: true;
        acceptedButtons: Qt.LeftButton | Qt.RightButton;
        onClicked: {
            slot.click();
        }
    }

    states: [
        State {
            name: "Selected"
            PropertyChanges {
                target: background;
                color: "#C6C6C6";
            }
        }
    ]

    function add_component(component) {
        if (component.length !== 0 && component !== "") {
            item.source = "qrc:///resources/textures/items/" + component + ".png";
        }
        else {
            remove_component();
        }
    }

    function remove_component() {
        item.source = "";
    }

    function reset_state() {
        slot.state = "";
    }
}
