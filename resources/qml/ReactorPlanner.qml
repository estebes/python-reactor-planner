import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Window {
    visible: true;
    id: reactor_planner;
    title: "Subock Reactor Planner";
    minimumWidth: 1280;
    minimumHeight: 640;
    maximumWidth: 1280;
    maximumHeight: 640;
    width: 1280;
    height: 640;

    // Background
    Item {
        id: background;
        anchors.fill: parent;
        implicitWidth: parent.width;
        implicitHeight: parent.height;

        Rectangle {
            width: parent.width;
            height: parent.height;
            anchors.centerIn: parent;
            color: "#000000";
        }

        Rectangle {
            width: parent.width - 6;
            height: parent.height - 6;
            anchors.centerIn: parent;
            color: "#C6C6C6";
        }

        Rectangle {
            width: parent.width - 12;
            height: parent.height - 12;
            x: 3;
            y: 3;
            color: "#FFFFFF";
        }

        Rectangle {
            width: parent.width - 12;
            height: parent.height - 12;
            x: 9;
            y: 9;
            color: "#555555"
        }

        Rectangle {
            width: parent.width - 18;
            height: parent.height - 18;
            anchors.centerIn: parent;
            color: "#C6C6C6";
        }
    }

    // Sidebar
    Item {
        id: sidebar;
        implicitWidth: 200;
        implicitHeight: parent.height - 52;
        x: 26;
        y: 26;

        // Layout
        Column {
            spacing: 10;

            // Simulate button.
            CustomButton {
                id: button_simulate;
                text: "Simulate";
                width: sidebar.width;
                height: 50;

                onClick: {
                    body_console.remove(0, body_console.length)
                    gui_handler.perform_reactor_simulation();
                }
            }

            // Clear button.
            CustomButton {
                id: button_clear;
                text: "Clear";
                width: sidebar.width;
                height: 50;

                onClick: {
                    body_console.remove(0, body_console.length)
                    for (var aux = 0; aux < 6 * 9; aux++) {
                        reactor_repeater.itemAt(aux).remove_component();
                    }
                }
            }

            // Share button.
            CustomButton {
                id: button_share;
                text: "Share";
                width: sidebar.width;
                height: 50;

                onClick: {
                    gui_handler.take_print_screen(true);
                }
            }

            // Quit button.
            CustomButton {
                id: button_quit;
                text: "Quit";
                width: sidebar.width;
                height: 50;
                y: sidebar.height - 50

                onClick: {
                    Qt.quit();
                }
            }
        }
    }

    // Body
    Item {
        id: body;
        implicitWidth: parent.width - 200 - 52 - 16;
        implicitHeight: parent.height - 26 - 26;
        x: 200 + 26 + 16;
        y: 26;

        property int selected_item: 0;

        Rectangle {
            implicitWidth: parent.width
            implicitHeight: parent.height
            color: "#C6C6C6";

            Column {
                spacing: 16;

                Rectangle {
                    id: body_reactor
                    width: 504
                    height: 336

                    GridLayout {
                        anchors.fill: parent;
                        flow: GridLayout.TopToBottom;
                        rows: 6;
                        columns: 9;
                        rowSpacing: 0;
                        columnSpacing: 0;

                        Repeater {
                            id: reactor_repeater;
                            model: 6 * 9;

                            delegate: Slot {
                                id: reactor_slot;
                                Layout.preferredWidth: parent.width / 9;
                                Layout.preferredHeight: parent.height / 6;

                                onLeftclick: {
                                    gui_handler.add_to_reactor(index, body.selected_item);
                                }

                                onRightclick: {
                                    reactor_slot.remove_component();
                                    gui_handler.remove_from_reactor(index);
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    id: body_screen;
                    width: 504;
                    height: 54;
                    color: "#000000";
                    border.color: "#373737";
                    border.width: 3;
                }

                Rectangle {
                    id: body_inventory;
                    width: 504;
                    height: 168;

                    GridLayout {
                        anchors.fill: parent
                        rows: 3
                        columns: 9
                        rowSpacing: 0
                        columnSpacing: 0

                        Repeater {
                            id: inventory_repeater;
                            model: 3 * 9

                            delegate: SelectionSlot {
                                id: inventory_slot;
                                Layout.preferredWidth: parent.width / 9
                                Layout.preferredHeight: parent.height / 3

                                onClick: {
                                    for (var aux = 0; aux < 3 * 9; aux++) {
                                        inventory_repeater.itemAt(aux).reset_state();
                                    }
                                    inventory_slot.state = "Selected";
                                    body.selected_item = index;
                                }
                            }
                        }

                        Component.onCompleted: {
                            reactor_planner.setup();
                        }
                    }
                }
            }

            Rectangle {
                anchors.top: parent.top;
                width: parent.width - 504 - 16;
                height: parent.height;
                x: 504 + 16
                color: "#373737";

                TextArea {
                    id: body_console;

                    FontLoader {
                        id: da_font;
                        source: "qrc:///resources/fonts/menu_font.ttf";
                    }

                    anchors.centerIn: parent;
                    width: parent.width - 6;
                    height: parent.height - 6;
                    font.family: da_font.name;
                    font.pointSize: 10;
                    readOnly: true;
                    menu: null;
                    frameVisible: false;

                    style: TextAreaStyle {
                            renderType: Text.NativeRendering;
                            backgroundColor: "#000000";
                            textColor: "#FFFFFF";
                        }
                }
            }
        }
    }

    Item {
        id: function_handler;
        objectName: "function_handler";

        // Add stuff to the output console
        function print_to_console(text) {
            body_console.append(text);
        }

        // Add component to the reactor grid
        function add_component_to_reactor(index, name, durability) {
            reactor_repeater.itemAt(index).add_component(name, durability);
        }

        // Update the durability on the reactor items
        function update_durability(index, durability) {
            reactor_repeater.itemAt(index).update_durability(durability);
        }
    }

    function setup() {
        var items = [ "", "single_uranium_rod", "dual_uranium_rod", "quad_uranium_rod", "single_mox_rod", "dual_mox_rod", "quad_mox_rod",
                      "reactor_plating", "heat_capacity_reactor_plating", "containment_reactor_plating", "heat_vent", "reactor_heat_vent",
                      "overclocked_heat_vent", "advanced_heat_vent", "component_heat_vent", "heat_exchanger", "reactor_heat_exchanger",
                      "component_heat_exchanger", "advanced_heat_exchanger", "rsh_condensator", "lzh_condensator",
                      "neutron_reflector", "thick_neutron_reflector", "iridium_neutron_reflector",
                      "coolant_cell_10k", "coolant_cell_30k",
                      "coolant_cell_60k" ]

        for (var index = 0; index < items.length; index++) {
            inventory_repeater.itemAt(index).add_component(items[index]);
        }
        inventory_repeater.itemAt(0).state = "Selected";
    }
}
