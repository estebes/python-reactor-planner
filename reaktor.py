import numpy
import copy
import math_util
from collections import deque
from damageable_component import DamageableComponent


# Reactor class
class Reactor:
    def __init__(self):
        # Reactor grid
        self.grid = numpy.empty((6, 9), dtype=object)

        # Reactor properties
        self.heat = 0
        self.max_heat = 10000

        # Time counters
        self.cycle_ticks = 0

        # Eu counters
        self.eu_output_tick = 0
        self.eu_output_cycle = 0
        self.eu_output_min = 0
        self.eu_output_max = 0

        # Deque with the simulation data
        self.console_output = deque()

    # toString equivalent
    def __str__(self):
        return str(self.grid)

    # Adding components to reactor
    def add_component(self, x_coord, y_coord, component):
        self.grid[x_coord, y_coord] = component

    # Adding components to reactor - GUI version
    def add_component_gui(self, index, component):
        coords = math_util.index_to_coords(index, 6)
        self.add_component(coords[0], coords[1], component)

    # Removing components from the reactor
    def remove_component(self, x_coord, y_coord):
        self.grid[x_coord, y_coord] = None

    # Removing components from the reactor - GUI version
    def remove_component_gui(self, index):
        coords = math_util.index_to_coords(index, 6)
        self.remove_component(coords[0], coords[1])

    def destroy_component(self, x_coord, y_coord, name):
        self.grid[x_coord, y_coord] = None
        self.console_output.append(
            "{} at coords ({},{}) was destroyed after {} seconds".format(
                name, x_coord, y_coord, str(int(self.cycle_ticks / 20))))

    # Get reactor's max heat
    def get_max_heat(self):
        return self.max_heat

    # Increase reactor's max heat
    def add_max_heat(self, amount):
        self.max_heat += amount

    # Get reactor's heat
    def get_heat(self):
        return self.heat

    # Increase reactor's heat
    def add_heat(self, amount):
        self.heat += amount

    # Set reactor's heat
    def set_heat(self, amount):
        self.heat = amount

    # Add to eu output
    def add_output_eu(self, amount):
        self.eu_output_tick += amount

    # Check for heat acceptor
    def check_heat_acceptor(self, x_target, y_target):
        if x_target not in range(6) or y_target not in range(9):
            return 0

        if self.grid[x_target, y_target] is None:
            return 0

        return self.grid[x_target, y_target].can_store_heat(self, x_target, y_target)

    # Check for heat acceptor
    def alter_heat_at(self, x_target, y_target, amount):
        if x_target not in range(6) or y_target not in range(9):
            return amount

        if self.grid[x_target, y_target] is None:
            return amount

        return self.grid[x_target, y_target].alter_heat(self, x_target, y_target, amount)

    # Pulse component
    def pulse_at(self, x_target, y_target, x_from, y_from):
        if x_target not in range(6) or y_target not in range(9):
            return 0

        if self.grid[x_target, y_target] is None:
            return 0

        return 1 if self.grid[x_target, y_target].pulse(self, x_target, y_target, x_from, y_from) else 0

    # Reactor simulation
    def simulate(self):
        # Create a copy the reactor grid
        grid_copy = copy.deepcopy(self.grid)

        # Reset values
        self.heat = 0
        self.max_heat = 10000
        self.cycle_ticks = 0
        self.eu_output_tick = 0
        self.eu_output_cycle = 0
        self.eu_output_min = 0
        self.eu_output_max = 0

        # Tick counter
        tick = 0

        while True:
            # Calculate the values to use for the next 20 ticks
            if tick == 0:
                self.max_heat = 10000
                self.eu_output_tick = 0
                for x in range(6):
                    for y in range(9):
                        if self.grid[x, y] is not None:
                            self.grid[x, y].process_chamber(self, x, y)

                # "It will be a smoking crater!" aka reactor explosion
                if self.heat >= self.max_heat:
                    self.console_output.append("It will be a smoking crater! - The reactor has exploded!!!")
                    break

                # Min EU/t
                if self.eu_output_tick != 0:
                    self.eu_output_min = min(self.eu_output_min, self.eu_output_tick)
                    if self.eu_output_min == 0:
                        self.eu_output_min = self.eu_output_tick

                # Max EU/t
                self.eu_output_max = max(self.eu_output_max, self.eu_output_tick)

            if self.eu_output_tick != 0:
                self.cycle_ticks += 1
                self.eu_output_cycle += self.eu_output_tick

            # Increase the tick counter
            tick += 1

            # 20 ticks have passed
            if tick == 20:
                tick = 0

            # Do while emulation
            if self.eu_output_tick <= 0:
                break

        # Add the simulation results to the deque
        self.console_output.append("Reactor Heat: " + str(self.heat))
        self.console_output.append("Total Output: " + str(round(float(self.eu_output_cycle), 3)))
        self.console_output.append(
            "Average Eu/T: " + str(round(float(
                0 if self.cycle_ticks == 0 else (self.eu_output_cycle / self.cycle_ticks)), 3)))
        self.console_output.append("Min Eu/T: " + str(round(float(self.eu_output_min), 3)))
        self.console_output.append("Max Eu/T: " + str(round(float(self.eu_output_max), 3)))
        self.console_output.append("Cycle duration (ticks): " + str(self.cycle_ticks))
        self.console_output.append("Cycle duration (seconds): " + str(int(self.cycle_ticks / 20)))

        output_grid = deque()
        for y in range(8, -1, -1):
            for x in range(5, -1, -1):
                if self.grid[x, y] is not None:
                    if isinstance(self.grid[x, y], DamageableComponent):
                        output_grid.append(1 - (self.grid[x, y].durability / self.grid[x, y].max_durability))
                    else:
                        output_grid.append(1)
                else:
                    output_grid.append(0)

        self.grid = grid_copy

        return output_grid, self.console_output
