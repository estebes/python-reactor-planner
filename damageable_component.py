import math_util
from collections import deque


# Reactor damageable component
class DamageableComponent:
    def __init__(self, name, max_durability):
        self.name = name
        self.durability = 0
        self.max_durability = max_durability

    def __str__(self):
        return "{} ({}/{})".format(self.name, self.durability, self.max_durability)

    def process_chamber(self, reactor, x_coord, y_coord):
        pass

    def pulse(self, reactor, x_coord, y_coord, x_pulse, y_pulse):
        return False

    def can_store_heat(self, reactor, x_coord, y_coord):
        return False

    def get_max_heat(self, reactor, x_coord, y_coord):
        return 0

    def get_current_heat(self, reactor, x_coord, y_coord):
        return 0

    def alter_heat(self, reactor, x_coord, y_coord, heat):
        return heat

    def influence_explosion(self, reactor):
        return 0.0


# Uranium rods
class AbstractUraniumRod(DamageableComponent):
    def __init__(self, name, rods):
        super().__init__(name, 20000)
        self.rods = rods

    def process_chamber(self, reactor, x_coord, y_coord):
        base_pulses = 1 + round(self.rods / 2)

        for iteration in range(self.rods):
            total_pulses = base_pulses

            for pulse in range(total_pulses):
                self.pulse(reactor, x_coord, y_coord, x_coord, y_coord)

            total_pulses += reactor.pulse_at(x_coord - 1, y_coord, x_coord, y_coord)
            total_pulses += reactor.pulse_at(x_coord + 1, y_coord, x_coord, y_coord)
            total_pulses += reactor.pulse_at(x_coord, y_coord - 1, x_coord, y_coord)
            total_pulses += reactor.pulse_at(x_coord, y_coord + 1, x_coord, y_coord)

            heat_generated = math_util.triangular_number(total_pulses) * 4

            heat_acceptors = deque()
            if reactor.check_heat_acceptor(x_coord - 1, y_coord):
                heat_acceptors.append(y_coord)
                heat_acceptors.append(x_coord - 1)
            if reactor.check_heat_acceptor(x_coord + 1, y_coord):
                heat_acceptors.append(y_coord)
                heat_acceptors.append(x_coord + 1)
            if reactor.check_heat_acceptor(x_coord, y_coord - 1):
                heat_acceptors.append(y_coord - 1)
                heat_acceptors.append(x_coord)
            if reactor.check_heat_acceptor(x_coord, y_coord + 1):
                heat_acceptors.append(y_coord + 1)
                heat_acceptors.append(x_coord)

            while heat_acceptors and heat_generated > 0:
                average_heat = int(heat_generated / int(len(heat_acceptors) / 2))
                heat_generated -= average_heat
                average_heat = reactor.alter_heat_at(heat_acceptors.pop(), heat_acceptors.pop(), average_heat)
                heat_generated += average_heat

            if heat_generated > 0:
                reactor.add_heat(heat_generated)

        self.durability += 1
        if self.durability >= self.max_durability - 1:
            reactor.destroy_component(x_coord, y_coord, self.name)

    def pulse(self, reactor, x_coord, y_coord, x_pulse, y_pulse):
        reactor.add_output_eu(5)
        return True


# Mox rods
class AbstractMoxRod(DamageableComponent):
    def __init__(self, name, rods):
        super().__init__(name, 10000)
        self.rods = rods

    def process_chamber(self, reactor, x_coord, y_coord):
        base_pulses = 1 + round(self.rods / 2)

        for iteration in range(self.rods):
            total_pulses = base_pulses

            for pulse in range(total_pulses):
                self.pulse(reactor, x_coord, y_coord, x_coord, y_coord)

            total_pulses += reactor.pulse_at(x_coord - 1, y_coord, x_coord, y_coord)
            total_pulses += reactor.pulse_at(x_coord + 1, y_coord, x_coord, y_coord)
            total_pulses += reactor.pulse_at(x_coord, y_coord - 1, x_coord, y_coord)
            total_pulses += reactor.pulse_at(x_coord, y_coord + 1, x_coord, y_coord)

            heat_generated = math_util.triangular_number(total_pulses) * 4

            heat_acceptors = deque()
            if reactor.check_heat_acceptor(x_coord - 1, y_coord):
                heat_acceptors.append(y_coord)
                heat_acceptors.append(x_coord - 1)
            if reactor.check_heat_acceptor(x_coord + 1, y_coord):
                heat_acceptors.append(y_coord)
                heat_acceptors.append(x_coord + 1)
            if reactor.check_heat_acceptor(x_coord, y_coord - 1):
                heat_acceptors.append(y_coord - 1)
                heat_acceptors.append(x_coord)
            if reactor.check_heat_acceptor(x_coord, y_coord + 1):
                heat_acceptors.append(y_coord + 1)
                heat_acceptors.append(x_coord)

            while heat_acceptors and heat_generated > 0:
                average_heat = int(heat_generated / int(len(heat_acceptors) / 2))
                heat_generated -= average_heat
                average_heat = reactor.alter_heat_at(heat_acceptors.pop(), heat_acceptors.pop(), average_heat)
                heat_generated += average_heat

            if heat_generated > 0:
                reactor.add_heat(heat_generated)

        self.durability += 1
        if self.durability >= self.max_durability - 1:
            reactor.destroy_component(x_coord, y_coord, self.name)

    def pulse(self, reactor, x_coord, y_coord, x_pulse, y_pulse):
        multiplier = reactor.get_heat() / reactor.get_max_heat()
        reactor.add_output_eu(5 * (4 * multiplier + 1))
        return True


# Heat vent
class AbstractHeatVent(DamageableComponent):
    def __init__(self, name, max_durability, side_vent, reactor_vent):
        super().__init__(name, max_durability)
        self.side_vent = side_vent
        self.reactor_vent = reactor_vent

    def process_chamber(self, reactor, x_coord, y_coord):
        if self.reactor_vent > 0:
            reactor_heat = reactor.get_heat()
            reactor_heat_drain = reactor_heat

            reactor_heat_drain = self.reactor_vent if reactor_heat_drain > self.reactor_vent else reactor_heat_drain

            reactor_heat -= reactor_heat_drain

            if reactor.alter_heat_at(x_coord, y_coord, reactor_heat_drain) > 0:
                return

            reactor.set_heat(reactor_heat)

        self_heat = reactor.alter_heat_at(x_coord, y_coord, -self.side_vent)

        if self_heat <= 0:
            pass
            # reactor.add_heat(self_heat + self.side_vent)

    def can_store_heat(self, reactor, x_coord, y_coord):
        return True

    def get_max_heat(self, reactor, x_coord, y_coord):
        return self.max_durability

    def get_current_heat(self, reactor, x_coord, y_coord):
        return self.durability

    def alter_heat(self, reactor, x_coord, y_coord, heat):
        self.durability += heat

        if self.durability >= self.max_durability:
            reactor.destroy_component(x_coord, y_coord, self.name)
        else:
            if self.durability < 0:
                heat = self.durability
                self.durability = 0
            else:
                heat = 0

        return heat


# Condensator
class AbstractCondensator(DamageableComponent):
    def __init__(self, name, max_durability):
        super().__init__(name, max_durability)

    def can_store_heat(self, reactor, x_coord, y_coord):
        return self.durability < self.max_durability

    def get_max_heat(self, reactor, x_coord, y_coord):
        return self.max_durability

    def get_current_heat(self, reactor, x_coord, y_coord):
        return self.durability

    def alter_heat(self, reactor, x_coord, y_coord, heat):
        if heat < 0:
            return heat

        aux = min(heat, self.max_durability - self.durability)

        heat -= aux
        self.durability += aux

        return heat


# Coolant cells
class AbstractCoolantCell(DamageableComponent):
    def __init__(self, name, max_durability):
        super().__init__(name, max_durability)

    def can_store_heat(self, reactor, x_coord, y_coord):
        return True

    def get_max_heat(self, reactor, x_coord, y_coord):
        return self.max_durability

    def get_current_heat(self, reactor, x_coord, y_coord):
        return self.durability

    def alter_heat(self, reactor, x_coord, y_coord, heat):
        self.durability += heat

        if self.durability >= self.max_durability:
            reactor.destroy_component(x_coord, y_coord, self.name)
        else:
            if self.durability < 0:
                heat = self.durability
                self.durability = 0
            else:
                heat = 0

        return heat


# Damageable neutron reflector
class AbstractDamageableReflector(DamageableComponent):
    def __init__(self, name, max_durability):
        super().__init__(name, max_durability)

    def pulse(self, reactor, x_coord, y_coord, x_pulse, y_pulse):
        reactor.pulse_at(x_pulse, y_pulse, x_coord, y_coord)

        self.durability += 1
        if self.durability >= self.max_durability - 1:
            reactor.destroy_component(x_coord, y_coord, self.name)

        return True

    def influence_explosion(self, reactor):
        return -1.0
