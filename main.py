import sys
import qrc
from gui_handler import GuiHandler
from PyQt5.QtCore import QUrl
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtGui import QGuiApplication


if __name__ == '__main__':
    # Create application
    app = QGuiApplication(sys.argv)

    # Create the qml engine
    engine = QQmlApplicationEngine()

    # Handle the signals/slots
    context = engine.rootContext()
    gui_handler = GuiHandler(app, engine)
    context.setContextProperty("gui_handler", gui_handler)

    # Load the qml file
    engine.load(QUrl("qrc:///resources/qml/ReactorPlanner.qml"))

    sys.exit(app.exec_())
