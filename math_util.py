def triangular_number(number):
    return int((number * number + number) / 2)


def coords_to_index(x_coord, y_coord, x_size):
    return coords_to_index_shifted(x_coord, y_coord, x_size, 0)


def coords_to_index_shifted(x_coord, y_coord, x_size, shifted):
    return x_coord + (y_coord * x_size) + shifted


def index_to_coords(index, x_size):
    return index_to_coords_shifted(index, x_size, 0)


def index_to_coords_shifted(index, x_size, shifted):
    index += shifted
    x_coord = int(index % x_size)
    y_coord = int((index - x_coord) / x_size)
    return x_coord, y_coord

