from collections import deque


# Reactor component
class Component:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    def process_chamber(self, reactor, x_coord, y_coord):
        pass

    def pulse(self, reactor, x_coord, y_coord, x_pulse, y_pulse):
        return False

    def can_store_heat(self, reactor, x_coord, y_coord):
        return False

    def get_max_heat(self, reactor, x_coord, y_coord):
        return 0

    def get_current_heat(self, reactor, x_coord, y_coord):
        return 0

    def alter_heat(self, reactor, x_coord, y_coord, heat):
        return heat

    def influence_explosion(self, reactor):
        return 0.0


# Reactor plating
class AbstractPlating(Component):
    def __init__(self, name, provided_heat, explosion_modifier):
        super().__init__(name)
        self.provided_heat = provided_heat
        self.explosion_modifier = explosion_modifier

    def process_chamber(self, reactor, x_coord, y_coord):
        reactor.add_max_heat(self.provided_heat)

    def influence_explosion(self, reactor):
        return self.explosion_modifier


# Component heat vent base class
class AbstractHeatSpreader(Component):
    def __init__(self, name, side_vent):
        super().__init__(name)
        self.side_vent = side_vent

    def process_chamber(self, reactor, x_coord, y_coord):
        heat_acceptors = deque()
        if reactor.check_heat_acceptor(x_coord - 1, y_coord):
            heat_acceptors.append(y_coord)
            heat_acceptors.append(x_coord - 1)
        if reactor.check_heat_acceptor(x_coord + 1, y_coord):
            heat_acceptors.append(y_coord)
            heat_acceptors.append(x_coord + 1)
        if reactor.check_heat_acceptor(x_coord, y_coord - 1):
            heat_acceptors.append(y_coord - 1)
            heat_acceptors.append(x_coord)
        if reactor.check_heat_acceptor(x_coord, y_coord + 1):
            heat_acceptors.append(y_coord + 1)
            heat_acceptors.append(x_coord)

        while heat_acceptors:
            self_heat = reactor.alter_heat_at(heat_acceptors.pop(), heat_acceptors.pop(), -self.side_vent)

            if self_heat <= 0:
                pass
                # reactor.add_heat(self_heat + self.side_vent)


# Non-damageable neutron reflector
class AbstractReflector(Component):
    def __init__(self, name):
        super().__init__(name)

    def pulse(self, reactor, x_coord, y_coord, x_pulse, y_pulse):
        reactor.pulse_at(x_pulse, y_pulse, x_coord, y_coord)
        return True

    def influence_explosion(self, reactor):
        return -1.0
