from component import *
from damageable_component import *


# Uranium rods
def single_uranium_rod():
    return AbstractUraniumRod("Fuel Rod (Uranium)", 1)


def dual_uranium_rod():
    return AbstractUraniumRod("Dual Fuel Rod (Uranium)", 2)


def quad_uranium_rod():
    return AbstractUraniumRod("Quad Fuel Rod (Uranium)", 4)


# Mox rods
def single_mox_rod():
    return AbstractMoxRod("Fuel Rod (MOX)", 1)


def dual_mox_rod():
    return AbstractMoxRod("Dual Fuel Rod (MOX)", 2)


def quad_mox_rod():
    return AbstractMoxRod("Quad Fuel Rod (MOX)", 4)


# Heat vents
def heat_vent():
    return AbstractHeatVent("Heat Vent", 1000, 6, 0)


def reactor_heat_vent():
    return AbstractHeatVent("Reactor Heat Vent", 1000, 5, 5)


def overclocked_heat_vent():
    return AbstractHeatVent("Overclocked Heat Vent", 1000, 20, 36)


def advanced_heat_vent():
    return AbstractHeatVent("Advanced Heat Vent", 1000, 12, 0)


def component_heat_vent():
    return AbstractHeatSpreader("Component Heat Vent", 4)


# Condensators
def rsh_condensator():
    return AbstractCondensator("RSH Condensator", 20000)


def lzh_condensator():
    return AbstractCondensator("LZH Condensator", 100000)


# Platings
def reactor_plating():
    return AbstractPlating("Reactor Plating", 1000, 0.95)


def heat_capacity_reactor_plating():
    return AbstractPlating("Heat-Capacity Reactor Plating", 2000, 0.99)


def containment_reactor_plating():
    return AbstractPlating("Containment Reactor Plating", 500, 0.9)


# Neutron reflectors
def neutron_reflector():
    return AbstractDamageableReflector("Neutron Reflector", 30000)


def thick_neutron_reflector():
    return AbstractDamageableReflector("Thick Neutron Reflector", 120000)


def iridium_neutron_reflector():
    return AbstractReflector("Iridium Neutron Reflector")


# Coolant cells
def coolant_cell_10k():
    return AbstractCoolantCell("10K Coolant Cell", 10000)


def coolant_cell_30k():
    return AbstractCoolantCell("30K Coolant Cell", 30000)


def coolant_cell_60k():
    return AbstractCoolantCell("60K Coolant Cell", 60000)
